
import string

# to create pseudo random values
import random 

#to input the password
password = input(" Enter the password : ")

#to remove garbage values
upper_case = 0
lower_case = 0
special = 0
digits = 0
length = 0

#to check if there is any upper case,lower case,special digits in the password 
upper_case = any([1 if c in string.ascii_uppercase else 0 for c in password])
lower_case = any([1 if c in string.ascii_lowercase else 0 for c in password])
special = any([1 if c in string.punctuation else 0 for c in password])
digits = any ([1 if c in string.digits else 0 for c in password])

#get the values to a variables
characters = [upper_case, lower_case, special, digits]

#get password length
length = len(password)

score = 0

#open the comman password list
with open('common.txt','r') as f:
    common = f.read().splitlines()

#check if the inputed password in the comman list
if password in common:
    print("\n password was found in a common list. Score: 0 / 7")
    exit()


#assign the score based on the password length
if length > 8:
    score += 1
if length > 12:
    score += 1
if length > 17:
    score += 1
if length > 20:
    score += 1
print(f"\n password length is {str(length)}, adding {str(score)} points!")

#assign score based on different chracters
if sum(characters) > 1:
    score += 1
if sum(characters) > 2:
    score += 1
if sum(characters) > 3:
    score += 1
print (f" Password has {str(sum(characters))} different character types, adding {str(sum(characters) - 1)} points!")

#decide if the password is weak ok or strong
if score < 4:
    print (f"\n the password is quite weak! Score: {str(score)} / 7")
    
elif score == 4:
    print (f"\n the password is OK! Score: {str(score)} / 7")
    
elif 4 < score <= 6:
    print (f"\n the password is pretty good! Score: {str(score)} / 7")
elif score > 6:
    print (f"\n the password is strong! Score: {str(score)} / 7")
    